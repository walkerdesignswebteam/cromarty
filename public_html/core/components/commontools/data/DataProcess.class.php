<?php
class DataProcess{
	static function arrayRowToCsv( array &$fields, $delimiter = ',', $enclosure = '"', $encloseAll = false, $nullToMysqlNull = false ) {
		$delimiter_esc = preg_quote($delimiter, '/');
		$enclosure_esc = preg_quote($enclosure, '/');

		$output = array();
		foreach ( $fields as $field ) {
			if ($field === null && $nullToMysqlNull) {
				$output[] = 'NULL';
				continue;
			}

			// Enclose fields containing $delimiter, $enclosure or whitespace
			if ( $encloseAll || preg_match( "/(?:${delimiter_esc}|${enclosure_esc}|\s)/", $field ) ) {
				$output[] = $enclosure . str_replace($enclosure, $enclosure . $enclosure, $field) . $enclosure;
			}
			else {
				$output[] = $field;
			}
		}

		return implode( $delimiter, $output );
	}
	static function arrayToCsv($a_dataRows,$delimiter=',') {
		$a_processedRowStrings=array();
		foreach($a_dataRows as $row){
			$a_processedRowStrings[]=self::arrayRowToCsv($row,$delimiter);
		}
		return implode($a_processedRowStrings,"\r\n");
	}

	static public function ParseCSVString($str)
	{
		ini_set('auto_detect_line_endings', true);
		//write the CSV string to a temporary file so that fgetcsv() can be used to process...
		$fp = tmpfile();
		fwrite($fp, $str);
		rewind($fp); //rewind to process CSV
		$data = array();
        while (($row = fgetcsv($fp, 5000,',')) !== FALSE){
			$data[] = $row;
        }
        fclose($fp);
	    return $data;
	}
        
        //if you want it to dynamically read and process all, just pass false to $a_requiredHeadKeys param
	static public function csvStringToArray($s_csvString,$a_requiredHeadKeys,&$a_errors){
		$a_rawCSV_data = self::ParseCSVString($s_csvString);

		//check all required columns exist and make array with indexes (as order should be variable)
		$headColumnsIndexes=array();
		$a_processErrors=array();
                if(!empty($a_requiredHeadKeys)){
                    foreach($a_requiredHeadKeys as $key){
                        $index = array_search($key,$a_rawCSV_data[0]);
                        if($index===false){
                            $a_processErrors[]='Data header column "'.$key.'" not found in csv';
                        }else{
                            if(in_array($key,$headColumnsIndexes)===true){
                                    echo 'Duplicate column "'.$key.'" in data.'; exit();
                            }
                            $headColumnsIndexes[$index]=$key;
                        }
                    }
                }else{
                    $index=0;
                    foreach($a_rawCSV_data[0] as $cell){
                        $headColumnsIndexes[$index]=$cell;
                        $index++;
                    }
                }

		array_splice($a_rawCSV_data,0,1);
		
		$aRet_byIndex=array();
		foreach($a_rawCSV_data as $a_line){
			$i_contentcount=0;
			foreach($a_line as $cell){
				if(empty($cell)===false){
					$i_contentcount++;
				}
			}
			if($i_contentcount>0){
				$a_tmpArry = array();
				for($iCnt=0;$iCnt<count($a_line);$iCnt++){
					//trim whitespace from around cell contents (some clients end up with all sorts of silly padding space around the value)
					if(empty($headColumnsIndexes[$iCnt])===false){
						$a_tmpArry[$headColumnsIndexes[$iCnt]]=trim($a_line[$iCnt]);
					}
				}
				$aRet_byIndex[]=$a_tmpArry;
			}
		}
		if(count($a_processErrors)>0){
			$a_errors=$a_processErrors;
			return false;
		}else{
			return $aRet_byIndex;
		}
	}
        static public function csvStringToArrayOfXPDO($s_csvString,$a_requiredHeadKeys,$s_XDPObjectName,$s_keyName,$keyValue,&$a_errors){
		$a_rawCSV_data = self::ParseCSVString($s_csvString);

		//check all required columns exist and make array with indexes (as order should be variable)
		$headColumnsIndexes=array();
		$a_processErrors=array();
		foreach($a_requiredHeadKeys as $key)
		{
			$index = array_search($key,$a_rawCSV_data[0]);
			if($index===false){
				$a_processErrors[]='Data header column "'.$key.'" not found in csv';
			}else{
				if(in_array($key,$headColumnsIndexes)===true){
					echo 'Duplicate column "'.$key.'" in data.'; exit();
				}
				$headColumnsIndexes[$index]=$key;
			}
		}
		array_splice($a_rawCSV_data,0,1);
		
		$aRet_byIndex=array();
		foreach($a_rawCSV_data as $a_line){
			$i_contentcount=0;
			foreach($a_line as $cell){
				if(empty($cell)===false){
					$i_contentcount++;
				}
			}
			if($i_contentcount>0){
                            $c = $this->modx->newQuery($s_XDPObjectName);
                            $c->where(array($s_keyName => $keyValue));
                            $o = $this->modx->getObject($s_XDPObjectName,$c);
                            
                            if($o === null){
                                $o = $this->modx->newObject($s_XDPObjectName);
                            }
				//$a_tmpArry = array();
				for($iCnt=0;$iCnt<count($a_line);$iCnt++){
					//trim whitespace from around cell contents (some clients end up with all sorts of silly padding space around the value)
					if(empty($headColumnsIndexes[$iCnt])===false){
						//$a_tmpArry[$headColumnsIndexes[$iCnt]]=trim($a_line[$iCnt]);
                                                $o->set($headColumnsIndexes[$iCnt], $a_line[$iCnt]);
					}
				}
                                    $aRet_byIndex[]=$o;                                            
                                $b_success = $o->save();
                                if(!$b_success){
                                    error_log('Failed to create object '.$s_XDPObjectName);
                                }
				//$aRet_byIndex[]=$a_tmpArry;
			}
		}
		if(count($a_processErrors)>0){
			$a_errors=$a_processErrors;
			return false;
		}else{
			return $aRet_byIndex;
		}
	}
	static public function sortMultiDimArray($a_arrayToSort,$s_key1,$i_dir1,$s_key2='',$i_dir2=''){
		// Obtain a list of columns
		foreach ($a_arrayToSort as $key => $row) {
		    $volume[$key]  = $row[$s_key1];
		    $edition[$key] = $row[$s_key2];
		}
		// Sort the data with volume descending, edition ascending
		// Add $data as the last parameter, to sort by the common key
		array_multisort($volume, $i_dir1, $edition, $i_dir2, $a_arrayToSort);
		return $a_arrayToSort;
	}
	
	static public function idKeyDataFromIndexedData($a_data,$s_idColumn){
		$a_ret=array();
		foreach($a_data as $val){
			if(isset($val[$s_idColumn])===true){
				$a_ret[$val[$s_idColumn]]=$val;
			}
		}
		return $a_ret;
	}
}