<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1, minimal-ui" name="viewport">

<?php
//Robots
if(strpos($_SERVER['HTTP_HOST'], 'dev.walkerdesigns.com.au')!==false){ echo '<meta name="robots" content="noindex,nofollow">'; }
?>

<base href="[[!++site_url]]" />
[[*description:equals=``:then=``:else=`<meta name="description" content="[[*description]]"/>`]]
<link rel="canonical" href="[[CommonTools? &cmd=`cannonicalURL`]]" />
[[CommonTools? &cmd=`noRobotsForHttps`]]

<!-- Facebook Open Graph meta data -->
<meta property="og:type" content="website" />
<meta property="og:title" content="[[*longtitle:equals=``:then=`[[*pagetitle]]`:else=`[[*longtitle]]`]]" />
<meta property="og:url" content="[[CommonTools? &cmd=`cannonicalURL`]]" />
[[*description:equals=``:then=``:else=`<meta property="og:description" content="[[*description]]"/>`]]
[[*socialImage:ne=``:then=`<meta property="og:image" content="[[++site_url]][[*socialImage]]" />`]]

<!-- Twitter meta data -->
<meta name="twitter:card" content="summary">
<meta name="twitter:title" content="[[*longtitle:equals=``:then=`[[*pagetitle]]`:else=`[[*longtitle]]`]]" />
<meta name="twitter:url" content="[[CommonTools? &cmd=`cannonicalURL`]]" />
[[*description:equals=``:then=``:else=`<meta name="twitter:description" content="[[*description]]"/>`]]
[[*socialImage:ne=``:then=`<meta name="twitter:image" content="[[++site_url]][[*socialImage]]" />`]]