<?php

//may want to require this is using formitbuilder forms (perhaps load as needed rather than in class head.
//require_once MODX_CORE_PATH.'components/formitbuilder/model/formitbuilder/FormItBuilder.class.php';
//Note - You can of course create a global reference to this object in order to run directly via PHP.
require_once 'Core.class.php';

class SiteCmd extends Core {

    function __construct(&$modx) {
        $this->modx = &$modx;
        parent::__construct($this->modx);
    }

    //[[!site_cmd? &json_options=`{"cmd":"output_mycontent"}`]]
    function requestOutput($json_options, $extraArg = NULL) {
        $s_ret = '';
        $o_commands = json_decode($json_options);
        switch ($o_commands->cmd) {
            case 'output_mycontent':
                $s_ret .= $this->output_mycontent();
                break;
            case 'outputForm_contactForm':
                $s_ret .= $this->outputForm_contactForm();
                break;
            case 'output_searchResults':
                $s_ret .= $this->output_searchResults();
                break;
            case 'output_SEOSummary': 
                $s_ret .= $this->output_SEOSummary();
                break;
            case 'output_registerActivation':
                $s_ret .= $this->output_registerActivation();
                break;
            case 'output_loginLanding':
                $s_ret .= $this->output_loginLanding();
                break;
            case 'output_forgotPassword':
                $s_ret .= $this->output_forgotPassword();
                break;
            case 'output_forgotPasswordReset':
                $s_ret .= $this->output_forgotPasswordReset();
                break;
            case 'output_logoutLink':
                $s_ret .= $this->output_logoutLink();
                break;
             case 'output_changePassWrapper':
                 $s_ret .= $this->output_changePassWrapper();
                break;
            default: return '--- ' . $o_commands->cmd . ' is not a valid command ---';
        }
        return $s_ret;
    }
    
    public function output_changePassWrapper() {
        if (isset($_REQUEST['logcp-success']) === true && $_REQUEST['logcp-success'] == 1) {
            return '<p>Thank you. Your password has now been changed.</p>';
        } else {
            return '<p>To reset your password please enter your old password below followed by the new password you would like to use.</p>
        [[!ChangePassword?
           &submitVar=`change-password`
           &placeholderPrefix=`cp.`
           &validateOldPassword=`1`
           &validate=`fgkthkghk:blank`
        ]]
        <div class="dialogPopupMessage"title="Change Password">[[!+cp.error_message]]</div>
        <form class="form change-password" action="[[~[[*id]]]]" method="post">
            <div>
                <input type="hidden" name="fgjkghlkkgfjf" value="" />
            </div>
            <div class="formSegWrap form-group">
                <label for="password_old">Old Password</label>
                <div class="elWrap">
                    <input class="form-control" type="password" id="password_old" name="password_old" value="[[+cp.password_old]]" />
                    <span class="after"></span>
                    <div class="errorContainer">
                        <label for="password_old" class="error">[[!+cp.error.password_old]]</label>
                    </div>
                </div>
            </div>
            <div class="formSegWrap form-group">
                <label for="password_new">New Password </label>
                <div class="elWrap">
                    <input class="form-control" type="password" name="password_new" id="password_new" value="[[+cp.password_new]]" />
                    <span class="after"></span>
                    <div class="errorContainer">
                        <label for="password_new" class="error">[[!+cp.error.password_new]]</label>
                    </div>
                </div>
            </div>
            <div class="formSegWrap form-group">
                <label for="password_new_confirm">Confirm Password </label>
                <div class="elWrap">
                    <input class="form-control" type="password" name="password_new_confirm" id="password_new_confirm" value="[[+cp.password_new_confirm]]" />
                    <span class="after"></span>
                    <div class="errorContainer">
                        <label for="password_new_confirm" class="error">[[!+cp.error.password_new_confirm]]</label>
                    </div>
                </div>
            </div>
            <div class="formSegWrap full">
                <div class="elWrap">
                    <input class="btn btn-primary" type="submit" name="change-password" value="Change Password" />
                </div>
            </div>
        </form>';
        }
    }
    
    function output_logoutLink(){
        return '[[~'.$this->resourceId_login.'? &service=`logout`]]';
    }
    
    public function loginRedirector() {
        $getLogout = $this->getVal('service');
        if ($getLogout == 'logout') {
            //ignore redirect if user is logging out.
        } else {
            if($this->isMemberLoggedIn()){
                $url = $this->modx->makeUrl($this->resourceId_registerMemberHome);
                $this->modx->sendRedirect($url);
            }
        }
    }
    
    public function output_forgotPassword(){
        return '[[!ForgotPassword? &resetResourceId=`'.$this->resourceId_forgotPassReset.'` &tpl=`_lgnForgotPassTpl` &emailTpl=`_lgnForgotPassEmailTpl`]]';
    }
    
    public function output_forgotPasswordReset(){
         $s_ret = '';
        $s_resetOutput = $this->modx->runSnippet('ResetPassword', array('loginResourceId' => $this->resourceId_login, 'tpl' => '_lgnResetPassTpl'));
        if (empty($s_resetOutput)) {
            $s_ret.='
            <h2>Password reset failed</h2>
            <p>If you have already followed this link, your account password may have already been reset.</p>
            <p>If you wish to reset your password again, please use the <a href="[[~' . $this->resourceId_forgotPass . ']]">forgotten password system</a>.</p>
            ';
        } else {
            $s_ret.=$s_resetOutput;
        }
        return $s_ret;
    }
    
    public function output_loginLanding() {
        
        //will redirect to member home if already logged in
        $this->loginRedirector();
        
        //Otherwise show login/register screen
        return '
        <div class="row">
            <div class="col-md-7">
                <h2>Login here if you have an account</h2>
                <p>If you already have an account simply enter your email address and password below to login to our secure portal. If you cannot remember your password, you can reset it using the forgotten password link below.</p>
                <div>[[!Login? &tplType=`modChunk` &loginTpl=`_lgnLoginTpl` &loginResourceId=`'.$this->resourceId_registerMemberHome.'`]]</div>
                <p>&nbsp;</p>
                <p><a href="[[~'.$this->resourceId_forgotPass.']]">Can\'t remember your password? Reset it here</a></p>
            </div>
            
            <div class="col-md-5">
               <h2 class="reg">Register for a new account here.</h2>
                '.$this->registerForm().'
            </div>
        </div>
        ';
    }
    
    
    public function output_registerActivation(){
        return '[[!ConfirmRegister? &redirectTo=`'.$this->resourceId_registerActivationSuccess.'` &errorPage=`'.$this->resourceId_registerActivationFail.'` &authenticate=`0`]]';
    }
    
    public function registerForm() {
  
        return '
[[!Register?
&activation=`1`
&activationEmailTpl=`_lgnActivateEmailTpl`
&activationEmailSubject=`Please activate your account!`
&activationResourceId=`'.$this->resourceId_registerActivation.'`
&activationttl=`10080`
&submittedResourceId=`'.$this->resourceId_registerThanks.'`
&postHooks=`register-auto-login` 
&submitVar=`registerSubmit`
&usergroups=`' . $this->group_members. '`
&validate=`nospam:blank,
email:required:email,
firstname:required,
lastname:required,
password:required:minLength=^8^,
password_confirm:password_confirm=^password^`
&placeholderPrefix=`reg.`
&usernameField=`email`
]]

<div class="register">
    <div class="registerMessage">[[!+reg.error.message]]</div>
	<form class="form" action="[[~[[*id]]]]" method="post">
		<div>
			<input type="hidden" name="nospam" value="[[!+reg.nospam]]" />
			<input type="hidden" name="registerSubmit" value="1" />

			<div class="form-group">
                            <label class="mainElLabel" for="firstname"><span class="mainLabel">First Name</span></label>
                            <div class="elWrap">
                                <input class="form-control" type="text" name="firstname" id="firstname" placeholder="First Name" value="[[!+reg.firstname]]" /> 
                                <div class="errorContainer"><label class="formiterror" for="firstname">[[!+reg.error.firstname]]</label></div>
                            </div>
			</div>
                        <div class="form-group">
                            <label class="mainElLabel" for="lastname"><span class="mainLabel">Last Name</span></label>
                            <div class="elWrap">
                                <input class="form-control" type="text" name="lastname" id="lastname" placeholder="Last Name" value="[[!+reg.lastname]]" /> 
                                <div class="errorContainer"><label class="formiterror" for="lastname">[[!+reg.error.lastname]]</label></div>
                            </div>
			</div>
                        <div class="form-group">
                            <label class="mainElLabel" for="reg-email"><span class="mainLabel">Email Address</span></label>
                            <div class="elWrap">
                                <input class="form-control" type="text" name="email" id="reg-email" value="[[!+reg.email]]" placeholder="Email Address" /> 
                                <div class="errorContainer"><label class="formiterror" for="reg-email">[[!+reg.error.email]]</label></div>
                            </div>
			</div>

			<div class="form-group">
                            <label class="mainElLabel" for="reg-password"><span class="mainLabel">Password</span></label>
                            <div class="elWrap">
                                <input class="form-control" type="password" name="password" id="reg-password" placeholder="Password" value="[[!+reg.password]]" /> 
                                <div class="errorContainer"><label class="formiterror" for="reg-password">[[!+reg.error.password]]</label></div>
                            </div>
			</div>
                        <div class="form-group">
                            <label class="mainElLabel" for="reg-password_confirm"><span class="mainLabel">Confirm Password</span></label>
                            <div class="elWrap">
                                <input class="form-control" type="password" name="password_confirm" id="reg-password_confirm" value="[[!+reg.password_confirm]]" /> 
                                <div class="errorContainer"><label class="formiterror" for="reg-password_confirm">[[!+reg.error.password_confirm]]</label></div>
                            </div>
			</div>
                        
                        <div class="elWrap">
                            <input type="submit" value="Register" class="btn btn-primary" name="Register">
                        </div>
			
		</div>
    </form>
</div>';
    }
        

    public function output_searchResults() {
        $s_searchVal = $this->getVal('search');
        $s_ret = '<div class="siteSearchForm">[[!SimpleSearchForm]]</div>';

        if (empty($s_searchVal) === false && strlen($s_searchVal) > 2) {

            $s_ret.= '<hr />

                    [[!SimpleSearch?
                        &toPlaceholder=`sisea.results`
                        &perPage=`500`
                    ]]

                    [[!If?
                        &subject=`[[+sisea.total]]`
                        &operator=`EQ`
                        &operand=`0`
                        &then=``
                        &else=`<h2>Found Pages ([[+sisea.total]])</h2>[[+sisea.results]]`
                    ]]

                    [[!If?
                        &subject=`[[+sisea.total]][[+sisea.places.total]]`
                        &operator=`EQ`
                        &operand=`00`
                        &then=`<h2>No results found</h2>`
                    ]]
            ';
        }
        return $s_ret;
    }

    function output_mycontent() {
        return 'blah';
    }

    function outputForm_contactForm() {
        require_once $this->modx->getOption('core_path', null, MODX_CORE_PATH) . 'components/jsonformbuilder/model/jsonformbuilder/JsonFormBuilder.class.php';

        //CREATE FORM ELEMENTS
        $o_fe_name = new JsonFormBuilder_elementText('name_full', 'Your Name');
        $o_fe_email = new JsonFormBuilder_elementText('email_address', 'Email Address');
        $o_fe_notes = new JsonFormBuilder_elementTextArea('comments', 'Comments', 5, 30);

        $o_fe_buttSubmit = new JsonFormBuilder_elementButton('submit', 'Contact Us', 'submit');
        
        //SET VALIDATION RULES
        $a_formRules = array();
        //Set required fields
        $a_formFields_required = array($o_fe_notes, $o_fe_name, $o_fe_email);
        foreach ($a_formFields_required as $field) {
            $a_formRules[] = new FormRule(FormRuleType::required, $field);
        }

        //Make email field require a valid email address
        $a_formRules[] = new FormRule(FormRuleType::email, $o_fe_email, NULL, 'Please provide a valid email address');

        //CREATE FORM AND SETUP
        $o_form = new JsonFormBuilder($this->modx, 'contactForm');
        $o_form->setRedirectDocument(8);
        $o_form->addRules($a_formRules);
        $o_form->setEmailToAddress($this->modx->getOption('emailsender'));
        $o_form->setEmailFromAddress($o_form->postVal('email_address'));
        $o_form->setEmailFromName($o_form->postVal('name_full'));
        $o_form->setEmailSubject('JsonFormBuilder Contact Form Submission - From: ' . $o_form->postVal('name_full'));
        $o_form->setEmailHeadHtml('<p>This is a response sent by ' . $o_form->postVal('name_full') . ' using the contact us form:</p>');
        $o_form->setJqueryValidation(true);
        $o_form->setPlaceholderJavascript('JsonFormBuilder_ContactForm');

        //ADD ELEMENTS TO THE FORM IN PREFERRED ORDER
        $o_form->addElements(
                array(
                    $o_fe_name, $o_fe_email, $o_fe_notes, $o_fe_buttSubmit
                )
        );

        // The form HTML will now be available via
        return $o_form->output();
        //This can be returned in a snippet or passed to any other script to handle in any way.
    }

    private function output_SEOSummary(){
        require_once MODX_BASE_PATH.'template/src/snippets/SEOSummary.class.php';
        $o_seoSummary = new SEOSummary($this->modx);
        $o_seoSummary->outputSummary();
    }
    
}
