<?php

class SEOSummary{

    private $modx;

    public function __construct(&$modx) {
        $this->modx = $modx;
    }

    public function outputSummary(){
        $a_fields = $this->getSummary();
        $this->outputCSV($a_fields);
    }

    private function getSummary(){
        $a_ret = array();
        $a_ret[] = array('Resource ID','Page Title','Page Title Length','Long Title','Long Title Length','Meta Description','Meta Description Length','URL');
        $c_resources = $this->modx->getCollection('modResource',array('published'=>1));
        foreach($c_resources as $resource){
            $url = $this->modx->makeUrl($resource->id,'','','full');
            $a_ret[] = array($resource->id,utf8_encode ($resource->pagetitle),strlen($resource->pagetitle),utf8_encode ($resource->longtitle),strlen($resource->longtitle),utf8_encode ($resource->description),strlen($resource->description),$url);
        }
        return $a_ret;
    }

    private function outputCSV($a_fields){
        $tempName = tempnam(MODX_CORE_PATH.'components/wd/temp/', '');
        $tempFile = fopen($tempName, 'r+');
        foreach($a_fields as $fields){
            fputcsv($tempFile, $fields);
        }
        $siteName = $this->modx->getOption('site_name');

        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Type: text/html; charset=utf-8');
        header('Content-Disposition: attachment; filename='.$siteName.' SEO Summary.csv');
        header('Content-Transfer-Encoding: binary');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize($tempFile));
        ob_clean();
        flush();

        readfile($tempName);
        exit;
    }

}
