[[!CommonTools? &cmd=`loadChunk` &name=`doctype`]]
<head>
    [[!CommonTools? &cmd=`loadChunk` &name=`title`]]
    [[!CommonTools? &cmd=`loadChunk` &name=`meta`]]
    [[!CommonTools? &cmd=`loadChunk` &name=`linkrel`]]
    [[!CommonTools? &cmd=`loadChunk` &name=`scriptsTop`]]
</head>
<body>
	<div class="container">
		[[!CommonTools? &cmd=`loadChunk` &name=`header`]]
		<div class="row">
			<main id="content" role="main" class="col-xs-8">
				<h1>[[*pagetitle]]</h1>
				[[*content]]
			</main>
			<aside class="col-xs-4">
				Sidebar
				[[Wayfinder? &startId=`[[UltimateParent]]` &level=`2`]]
			</aside>
		</div><!--/.row -->
		[[!CommonTools? &cmd=`loadChunk` &name=`footer`]]	
	</div><!-- /container -->
	[[!CommonTools? &cmd=`loadChunk` &name=`scriptsBottom`]]
</body>
</html>