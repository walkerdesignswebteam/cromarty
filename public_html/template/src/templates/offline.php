[[!CommonTools? &cmd=`loadChunk` &name=`doctype`]]
<head>
    [[!CommonTools? &cmd=`loadChunk` &name=`title`]]
    [[!CommonTools? &cmd=`loadChunk` &name=`meta`]]
    [[!CommonTools? &cmd=`loadChunk` &name=`linkrel`]]
    [[!CommonTools? &cmd=`loadChunk` &name=`scriptsTop`]]
</head>
<body>
	<div class="container">
		<div class="row">
			<main id="content" role="main" class="col-xs-12 text-center">
				<h1>[[*pagetitle]]</h1>
				[[*content]]
			</main>
		</div><!--/.row -->
	</div><!-- /container -->
	[[!CommonTools? &cmd=`loadChunk` &name=`scriptsBottom`]]
</body>
</html>