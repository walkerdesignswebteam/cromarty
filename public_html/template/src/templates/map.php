[[!CommonTools? &cmd=`loadChunk` &name=`doctype`]]
<head>
    [[!CommonTools? &cmd=`loadChunk` &name=`title`]]
    [[!CommonTools? &cmd=`loadChunk` &name=`meta`]]
    [[!CommonTools? &cmd=`loadChunk` &name=`linkrel`]]
    [[!CommonTools? &cmd=`loadChunk` &name=`scriptsTop`]]
</head>
<body>
	<div class="container">

		[[!CommonTools? &cmd=`loadChunk` &name=`header`]]

		<div class="row">

			<header role="banner" class="col-xs-12">
				<h1>[[*pagetitle]]</h1>
			</header>

			<main id="content" role="main" class="col-xs-12">
				[[*content]]
                <div id="store_map"></div>
            </main>

            <aside class="col-xs-12">
                aside here
            </aside>
            [[!CommonTools? &cmd=`loadChunk` &name=`footer`]]	
        </div><!-- /container -->
    </div><!-- /container -->
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=true"></script>
    <script type="text/javascript" src="template/js/StoreMap.class.js"></script>        
    <script type="text/javascript">
    $(document).ready(function(){
        [[-
        use this one if you want to use custom google map styles
        var customStyle = [ { "featureType": "road.highway", "stylers": [ { "hue": "#ff0000" } ] },{ "featureType": "road.local", "stylers": [ { "hue": "#ff0000" } ] },{ "featureType": "road.arterial", "stylers": [ { "hue": "#ff0000" } ] },{ "featureType": "landscape.man_made", "stylers": [ { "saturation": -100 } ] },{ "featureType": "landscape.natural", "stylers": [ { "saturation": -100 } ] },{ "featureType": "poi.park", "stylers": [ { "saturation": -100 } ] },{ },{ "featureType": "water", "stylers": [ { "hue": "#ff0000" } ] },{ "featureType": "poi", "stylers": [ { "hue": "#ff0000" } ] },{ } ];
        var map = new StoreMap('store_map',-41.447643,147.139475,16,customStyle);
        ]]
        var map = new StoreMap('store_map',-41.447643,147.139475,16);
        map.addSinglePoint(-41.447643,147.139475,'Business Name','template/images/location-marker.png',30,40,0,0,10,40);
        
        [[-
        Use this one if you want to have an infowindow
        var infoContent = '<div class="infowindowCont"><b>Walker Designs<br />134 St John Street,<br />Launceston, Tas 7250</div>';    
        map.addSinglePointWithInfoWindow(-41.440834,147.141519,'Business Name','template/images/location-marker.png',30,40,0,0,10,40,infoContent);
        ]]
        
    });
</script>
[[!CommonTools? &cmd=`loadChunk` &name=`scriptsBottom`]]
</body>
</html>